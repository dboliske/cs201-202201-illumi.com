package labs.lab2;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-02), name of program (Print out a square)

import java.util.Scanner;

public class printOutSquare_Exercise1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner for the user input
		
		System.out.print("Input the size of the square: ");
		
		int size = Integer.parseInt(input.nextLine()); // initialize the square size with the integer data type
		int row = 1;
		int col = 1;
		
		input.close();
		
		while (row <= size) { // draw the row of the square
			 col = 1; // reset the column number
			 while (col <= size) { // draw the column of the square
				 System.out.print("* ");
				 col++;
			 }
			 System.out.println();
			 row++;
		}

	}

}
