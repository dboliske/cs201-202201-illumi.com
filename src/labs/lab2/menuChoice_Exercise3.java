package labs.lab2;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-02), name of program (Adder and multiplier)

import java.util.Scanner;

public class menuChoice_Exercise3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner for user input
		boolean running = true; // initialize the flag for the loop below
	
		while (running) { // a flag controlled loop, the loop keeps processing data until the value of running changes
			
			// print out the menu
			System.out.println("Choose the option below to continue...");
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			
			String option = input.nextLine();
			
			switch(option) {
			case "1": // the first option: say hello
				System.out.println("Hello!");
				System.out.println();
				break;
			
			case "2": // the second option: add 2 numbers
				System.out.print("Enter 1st number: ");
				String input1 = input.nextLine();
				double add1 = Double.parseDouble(input1);
				System.out.print("Enter 2nd number: ");
				String input2 = input.nextLine();
				double add2 = Double.parseDouble(input2);
				System.out.println(add1 + " + " + add2 + " = " + (add1+add2));
				System.out.println();
				break;
				
			case "3": // the third option: the multiplier
				System.out.print("Enter 1st number: ");
				input1 = input.nextLine();
				add1 = Double.parseDouble(input1);
				System.out.print("Enter 2nd number: ");
				input2 = input.nextLine();
				add2 = Double.parseDouble(input2);
				System.out.println(add1 + " * " + add2 + " = " + (add1*add2));
				System.out.println();
				break;
			
			case "4": // exit the program
				System.out.println("Goodbye");
				running = false; // modify the flag controlled variable
				break;
				
			default:
				System.out.println("Invalid input, try again!");
				System.out.println();
			}
		
		}
		
		input.close();
		
	}

}
