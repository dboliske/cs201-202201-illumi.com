package labs.lab2;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-02), name of program (Average score of exams)

import java.util.Scanner;

public class examGrades_Exercise2 {

	public static void main(String[] args) {
	      Scanner input = new Scanner(System.in);  // create a scanner for the user input
	      
	      String score = ""; // the score variable for each exam
	      double totalScore = 0;  // total score of all the exams
	      int index = 0; // the number of the exams
	      
	      System.out.println("Compute the average score of all the exams. Enter -1 when you finished entering all the scores.");
          
	      // handle an unspecified number of grades.
	      do {
	            System.out.print("Enter the score of the #" + (index + 1) + " course: ");
	            score = input.nextLine();
	            totalScore = Double.parseDouble(score) + totalScore;
	            index = index + 1;
	      	} while (!score.equals("-1"));

	      input.close();
	      
          // compute the average score
	      System.out.println("Your average score is " + (totalScore + 1)/(index - 1)); 
		
		

	}

}
