package labs.lab7;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-04), name of program (Selection Sort)

public class Exercise3 {
	
	// Selection Sort method
	public static double[] sort(double[] fpArray) {
		for (int i=0; i<fpArray.length-1; i++) {
			int min = i;
			for(int j=i+1; j<fpArray.length; j++) {
				if(fpArray[j] < fpArray[min]) {
					min = j;
				}
			}
			
			if(min != i) {
				double temp = fpArray[min];
				fpArray[min] = fpArray[i];
				fpArray[i] = temp;
			}
		}
		return fpArray;
	}

	public static void main(String[] args) {
		double[] fpArray = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		fpArray = sort(fpArray);
		
		for (double f : fpArray) {
			System.out.print(f + " ");
		}
	}

}
