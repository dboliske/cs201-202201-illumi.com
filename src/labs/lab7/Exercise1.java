package labs.lab7;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-04), name of program (Bubble Sort)

public class Exercise1 {
	
	// bubble sort method
	public static int[] sort(int[] array) {
		boolean done = false;
		
		do {
			done = true;
			for(int i=0; i<array.length - 1; i++) {
				if (array[i+1] < array[i]) {
					int temp = array[i+1];
					array[i+1] = array[i];
					array[i] = temp;
					done = false;
				}
			}
			
		} while(!done);
		
		return array;
	}

	public static void main(String[] args) {
		int[] intArray = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		intArray = sort(intArray);
		
		for (int i : intArray) {
			System.out.print(i + " ");
		}
	}

}
