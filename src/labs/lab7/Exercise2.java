package labs.lab7;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-04), name of program (Insertion Sort)

public class Exercise2 {
	
	// Insertion Sort method
	public static String[] sort(String[] array) {
		
		for(int j=1; j<array.length; j++) {
			int i = j;
			while (i>0 && array[i].compareTo(array[i-1])<0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] strArray = {"cat", "fat", "dog", "apple", "bat", "egg"};
		strArray = sort(strArray);
		
		for (String s : strArray) {
			System.out.print(s + " ");
		}

	}

}
