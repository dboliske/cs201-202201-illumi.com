package labs.lab7;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-04), name of program (Binary Search)

import java.util.Scanner;



public class Exercise4 {
	
	// Insertion Sort method
	public static String[] sort(String[] array) {
		
		for(int j=1; j<array.length; j++) {
			int i = j;
			while (i>0 && array[i].compareTo(array[i-1])<0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}
	
	// Binary Search with recursive method
	public static int binarySearch(String[] array, int start, int end, String value) {
		
		int middle = (start + end)/2;
		int pos = -1;
		
		// base case
		if(start > end) {
			return pos;
		}
		
		// general case
		if(value.compareToIgnoreCase(array[middle]) < 0) {
			pos = binarySearch(array, start, middle - 1, value);
			return pos;
		} else if (value.compareToIgnoreCase(array[middle]) > 0) {
			pos = binarySearch(array, middle + 1, end, value);
			return pos;
		}
		
		return middle;
		
//		if (start == end) {
//			return start;
//		}
//	
//		if (value.compareToIgnoreCase(array[middle]) < 0) {
//			pos = binarySearch(array, start, middle - 1, value);
//			return pos;
//		} else if (value.compareToIgnoreCase(array[middle]) > 0) {
//			pos = binarySearch(array, middle + 1, end, value);
//			return pos;
//		}
//	
//		return -1;
		
	}
	

	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		lang = sort(lang); // sort the array if neccessary
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a keyword to search: ");
		String value = input.nextLine();
		int index = binarySearch(lang, 0, lang.length - 1, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at position " + index + " after being sorted.");
		}
		
		input.close();

	}

}
