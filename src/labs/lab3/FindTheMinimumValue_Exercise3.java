package labs.lab3;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-13), name of program (Find the minimum value)

public class FindTheMinimumValue_Exercise3 {

	public static void main(String[] args) {
		
		// initialize the array
		int[] value = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		int counter = 0;
		int min = value[counter];
		
		// compare the numbers next to each other one by one
		for (counter=0; counter<value.length - 1; counter++) {
			if (value[counter + 1] < value[counter]) {
				min = value[counter + 1];
			}
			
		}
		
		// print out the result
		System.out.println("The given array is {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}.");
		System.out.println("The smallest number of this array is " + min);
		
		


	}

}
