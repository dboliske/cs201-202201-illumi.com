package labs.lab3;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-13), name of program (Average grade for a class)

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ClassAverageGrades_Exercise1 {

	public static void main(String[] args) throws IOException {
		
		// create the file object
		File f = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(f);

		// create the array to store all the students' exam grades
		int[] grades = new int[14];
		int counter = 0;
		double total = 0;
		
		// import all the grades number from the .csv file
		while(input.hasNextLine()) {
			String[] values = input.nextLine().split(",");
			grades[counter] = Integer.parseInt(values[1]);
			counter++;
		}
		
		input.close();
		
		// Calculate the total score of the class
		for(int i=0; i<grades.length; i++) {
			total = grades[i] + total;
		}
		
		// print out the average grade for the class
		System.out.println("The average grade for the class is " + (total/14));
		
	}

}
