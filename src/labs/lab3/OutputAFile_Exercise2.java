package labs.lab3;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-13), name of program (Output user's inputs)

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class OutputAFile_Exercise2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); // create a scanner for user inputs
		
		int counter = 0;
		boolean running = true;
		String[] numbers = new String[2]; //initialize the array for storing user inputs
		
		System.out.println("Recording input numbers. Enter 'done' when finished.");
		
		// prompt the user to input continuously until the word "done" is detected
		while (running) {
			System.out.print("Enter the number #" + (counter+1) + ":");
			numbers[counter] = input.nextLine();
			
			if (!numbers[counter].equalsIgnoreCase("done")) {

				// resize the array
				if (counter>=1) {
					String[] more = new String[numbers.length + 1];
					for (int i=0; i<numbers.length;i++) {
						more[i] = numbers[i];
					}
					numbers = more;
					more = null;
				}
				counter++;
				
			} else {
				
				// get rid of the last element of the array, which is the word "done"
				String[] withoutDone = new String[counter];
				for (int i=0; i<withoutDone.length; i++) {
					withoutDone[i] = numbers[i];
				}
				numbers = withoutDone;
				withoutDone = null;
				
				running = false;
				
				// output the array to a file
				System.out.print("Save the data as a file. Name the file (eg. output.txt): ");
				String fileName = input.nextLine();
				try {
					FileWriter f = new FileWriter("src/labs/lab3/"+ fileName);
					
					for (int i=0; i<numbers.length; i++) {
						f.write(numbers[i] + "\n");
					}
					f.flush();
					f.close();
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
				
				System.out.println("Done! Check the file saved at scr/labs/labs3");
			}

		}	
		
		input.close();
		

	}

}
