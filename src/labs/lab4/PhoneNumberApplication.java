package labs.lab4;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (PhoneNumber Application Class)

public class PhoneNumberApplication {

	public static void main(String[] args) {
		// instantiate 2 instances of PhoneNumber
		PhoneNumber myNumber = new PhoneNumber();
		PhoneNumber hisNumber = new PhoneNumber("00","021","1234567");
		
		// display the values of each object by calling the toString method
		System.out.println("My phone number is " + myNumber.toString());
		System.out.println("His number is " + hisNumber.toString());

	}

}
