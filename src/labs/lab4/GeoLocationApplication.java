package labs.lab4;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (GeoLocation Application Class)

public class GeoLocationApplication {
	
	
	
	public static void main(String[] args) {
		
		// write an application class that instantiates two instances of GeoLocation.
		// One instance should use the default constructor, and the other should use
		// the non-default constructor.
		GeoLocation gultOfGuinea = new GeoLocation();
		GeoLocation beijing = new GeoLocation(90,116);
		
		// Display the values of the instance variables by calling the accessor methods.
		System.out.println("The latitude and longitude of Gult of Guinea is (" + gultOfGuinea.getLat() + ", " + gultOfGuinea.getLng() + ")");
		System.out.println("The Latitude and longitude of Beijing is (" + beijing.getLat() + ", " + beijing.getLng() + ")");
	}
}
