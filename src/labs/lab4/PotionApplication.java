package labs.lab4;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (Potion Application Class)

public class PotionApplication {

	public static void main(String[] args) {
		
		// instantiate 2 instances of Potion
		Potion potion1 = new Potion();
		Potion potion2 = new Potion("Polyjuice", 8);
		
		// display the value of each object by calling the toString method
		System.out.println("The first potion's name and strength is " + potion1.toString());
		System.out.println("The second potion's name ane strength is " + potion2.toString());
	}

}
