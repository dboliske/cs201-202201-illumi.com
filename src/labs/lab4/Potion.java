package labs.lab4;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (Potion Class)

public class Potion {
	
	// create 2 instance variables, name and strength
	private String name;
	private double strength;
	
	// write the default constructor
	public Potion() {
		name = "Veritaserum";
		strength = 10;
	}
	
	// write the non-default constructor
	public Potion(String name, double strength) {
		this.name = name;
		this.strength = 10;
		setStrength(strength);
	}
	
	// write 2 accessor method, one for each instance variable
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	// write 2 mutator method, one for each instance variable
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		if (validStrength(strength)) {
			this.strength = strength;
		}
	}
	
	// write the toString method
	public String toString() {
		return name + ", " + strength;
	}
	
	// write a method that will return true if strength is between 0 and 10
	public boolean validStrength(double strength) {
		if (strength <= 10 && strength >= 0) {
			return true;
		}
		
		return false;
	}
	
	// write the equals method
	public boolean equals(Potion p) {
		if (!name.equals(p.name)) {
			return false;
		} else if (strength != p.strength) {
			return false;
		}
		
		return true;
	}

}
