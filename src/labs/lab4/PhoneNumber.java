package labs.lab4;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (PhoneNumber Class)

public class PhoneNumber {
	
	// Create 3 instance variables, all of which should be Strings
	private String countryCode;
	private String areaCode;
	private String number;
	
	// write the default constructor and the non-default constructor
	public PhoneNumber() {
		countryCode = "86";
		areaCode = "021";
		number = "0000000";
	}
	
	public PhoneNumber(String countryCode, String areaCode, String number) {
		this.countryCode = countryCode;
		this.areaCode = "021";
		setAreaCode(areaCode);
		this.number = number;
		setNumber(number);
	}
	
	// wirte 3 accessor methods, one for each instance variable
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	// write 3 mutator methods, one for each instance variable
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) {
		if(validAreaCode(areaCode)) {
			this.areaCode = areaCode;
		}
	}
	
	public void setNumber(String number) {
		if (validNumber(number)) {
			this.number = number;
		}
	}
	
	// write the toString method
	public String toString() {
		return "+" + countryCode + " " + areaCode + number;
	}
	
	// write a method that will return true if the areCode is 3 characters long
	public boolean validAreaCode(String areaCode) {
		if (areaCode.length() == 3) {
			return true;
		}
		
		return false;
	}
	
	// write a method that will return true if the number is 7 characters long
	public boolean validNumber(String number) {
		if (number.length() == 7) {
			return true;
		}
		
		return false;
	}
	
	// write the equals method
	public boolean equals(PhoneNumber p) {
		if (this.countryCode != p.countryCode) {
			return false;
		} else if (this.areaCode != p.areaCode) {
			return false;
		} else if (this.number != p.number) {
			return false;
		}
		
		return true;
	}

}
