package labs.lab0;

public class Self_Introduction_Lab0 {

	public static void main(String[] args) {
		
		//Create the name and birthday variables in case it can be used for more than this task.
		String name = "Illumi Huang";
		String birthdate = "Jan. 15th, 1986";
		
		//Print out the sentence.
		System.out.println("My name is " + name + " and my birthdate is " + birthdate + ".");
		


	}

}
