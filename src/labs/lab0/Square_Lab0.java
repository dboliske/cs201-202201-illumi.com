package labs.lab0;

import java.util.Random;

public class Square_Lab0 {

	public static void main(String[] args) {
		// Firstly, the square size can be random. Secondly, the square is designed to be hollow. 
		// Given the limitation of char data type, Loops structure should be used here.
		// The main idea is to form the square by repeating a character. And the square will be hollow, so the part excluding the 1st and the last line should be a little different.		
		
		//Randomize the square size
		Random rand = new Random();
		int squareSize = rand.nextInt(10)+4;

		//Draw the first line of the square
		for(int i = 0; i < squareSize; i++) {
		    System.out.print("+");
		}

		
		//Draw the rest of the square except the last row
		for (int k = 0; k < squareSize/2 - 1; k++) { //Assume that the line pitch is 2x wider than the character pitch of the same line
			System.out.println();  //Start a new line
			System.out.print("+"); //the first character of the new line
			
			for (int j = 0; j < squareSize - 2; j++) { //Fill the square with the space character considered the square is designed to be hollow
				System.out.print(" "); //Start another new line, and repeat this process
			}
			
			System.out.print("+"); //the last character of the new line
		}

		//Draw the last row of the square
		System.out.println();
		for (int i = 0; i < squareSize; i++) {
		    System.out.print("+");
		}

		}

	}
