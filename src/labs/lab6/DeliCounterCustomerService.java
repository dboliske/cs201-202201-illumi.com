package labs.lab6;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-03-20), name of program (Deli Counter Customer Service)

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounterCustomerService {

	public static void main(String[] args) {
		menu();
	}
	
	public static void menu() {
		Scanner input = new Scanner(System.in);
		ArrayList<String> data = new ArrayList<String>();
		
		boolean done = false;
		String[] options = {"Add customer to queue", "Help customer", "Exit"};
		do {
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1)+". " + options[i]);
			}
			System.out.print("Select a service above (1-3): ");
			
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // Add customer to queue
					data = addCustomer(data, input);
					System.out.println("This customer's potion in the quene is: " + "No. " + data.size());
					System.out.println();
					break;
				
				case "2": // Help customer
					helpCustomer(data);
					break;
				
				case "3": // Exit the menu
					System.out.println("Goodbye.");
					done = true;
					break;
					
				case "4": // A hidden option
					listCustomers(data);
					System.out.println();
					break;
				
				default:
					System.out.println("Invalid input. Try again.");
					System.out.println();
			}
		} while(!done);
	}
	
	
	// Add a customer to the ArrayList
	public static ArrayList<String> addCustomer(ArrayList<String> data, Scanner input) {
		System.out.print("Enter the customer's Name: ");
		String name = input.nextLine();
		data.add(name);
		
		return data;
	}
	
	// List all the customers in the queue for the hidden option
	public static void listCustomers(ArrayList<String> data) {
		if (data.size() > 0) {
			System.out.println("Now list all the customers in the queue: ");
			for (int i=0; i<data.size(); i++) {
				System.out.println("(" + (i+1) + ") " + data.get(i));
			}
		} else {
			System.out.println("No customer's in the queue.");
		}
	}
	
	// Remove the customer at position 0
	public static ArrayList<String> helpCustomer(ArrayList<String> data) {
		String toRemove;
		try {
			toRemove = data.remove(0);
			System.out.println("The customer '" + toRemove + "' has been removed.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("No customer can be removed.");
			System.out.println();
		}

		return data;
	}

}
