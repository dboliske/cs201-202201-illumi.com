package labs.lab5;

public class CTAStation extends GeoLocation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		
		super();
		name = "Harlem";
		location = "elevated";
		wheelchair = true;
		open = true;
	}
	
	public CTAStation(String name, double lat, double lng, String location, 
			boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return "Station: " + name + ", Location: " + location + ", Coordinate: " + super.toString() 
		+ ", Wheelchair: " + wheelchair + ", Open: " + open;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation c = (CTAStation)obj;
		if (this.name != c.name) {
			return false;
		} else if (this.location != c.location) {
			return false;
		} else if (this.wheelchair != c.wheelchair) {
			return false;
		} else if (this.open != c.open) {
			return false;
		}
		
		return true;
	}

}
