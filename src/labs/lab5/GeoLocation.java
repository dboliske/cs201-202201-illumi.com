package labs.lab5;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-20), name of program (GeoLocation Class)

public class GeoLocation {
	
	private double lat;
	private double lng;
	
	// write the default constructor
	public GeoLocation() {
		lat = 41.88706;
		lng = -87.80486;
	}
	
	// write the non-default constructor
	public GeoLocation(double lat, double lng) {
		this.lat = 0;
		setLat(lat);
		this.lng = 0;
		setLng(lng);
	}
	
    // write 2 accessor methods	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	// write 2 mutator methods
	public void setLat(double lat) {
		if (validLat(lat)) {
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) {
		if (validLng(lng)) {
			this.lng = lng;
		}	
	}
	
	// write the toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	// write a method that will return true if the latitude is between -90 and +90
	public boolean validLat(double lat) {
		if(lat <= 90 && lat >= -90) {
			return true;
		}
		return false;
	}
	
	// write a method that will return true if the longitude is between -180 and + 180
	// same as the validLat()
	public boolean validLng(double lng) {
		if(lng <= 180 && lng >= -180) {
			return true;
		}
		return false;
	}
	
	// write the equals method
	public boolean equals(GeoLocation g) {
		if (this.lat != g.getLat()) {
			return false;
		} else if (this.lng != g.getLng()) {
			return false;
		}
		return true;
	}
	
	// write the calcDistance method
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.lat - g.lat, 2) + Math.pow(this.lng - g.lng, 2));
	}
	
	// write another calcDistance method
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}

}
