package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {

	public static void main(String[] args) {
		// Read in the file
		CTAStation[] station = readFile("src/labs/lab5/CTAStops.csv");
		
		// Display the user menu
		menu(station);
	}
	
	
	public static CTAStation[] readFile(String file) {
		CTAStation[] station = new CTAStation[0];
		int count = 0;
		
		try {
			// Read in the file
			File f = new File(file);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] values = line.split(",");
				
				if(Character.isDigit(values[1].charAt(0))) { // Skip the first line
					CTAStation a = new CTAStation(values[0], Double.parseDouble(values[1]), 
							Double.parseDouble(values[2]), values[3], 
							Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
					
					// Pass in all the values, 
					// and resize the station array when iterating each time
					station = resize(station, station.length + 1);
					station[count] = a;
					count++;
				}	
			}
			input.close();
			
		} catch (Exception e) {
			System.out.println("Error occurred reading in file...");
			System.out.println();
		}
		
		return station;
	}
	
	
	public static void menu(CTAStation[] station) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
			// main user menu
			System.out.println("1. Display Station Name");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Enter the option above (1-4): ");
			
			String in = input.nextLine();
			
			// user's choice corresponds to each option and action
			switch (in) {
				case "1":
					displayStationNames(station);
					System.out.println();
					break;
				case "2":
					displayByWheelchair(input, station);
					System.out.println();
					break;
				case "3":
					displayNearest(input, station);
					System.out.println();
					break;
				case "4":
					System.out.println("Goodbye!");
					done = true;
					break;
				default:
					System.out.println("Invaid input! Try again.");
					System.out.println();
			}
		} while(!done);
		
	}
	
	
	public static void displayStationNames(CTAStation[] station) {
		// Iterates through the array and print out all the names of the station.
		if (station.length != 0) {
			for (int i=0; i < station.length; i++) {
				System.out.println(station[i].getName());
			}
		} else {
			System.out.println("No data is available...");
		}
	}
	
	
	public static void displayByWheelchair(Scanner input, CTAStation[] station) {
		
		boolean done = false;
		CTAStation[] wheelChairGroup = new CTAStation[2];
		int count = 0;
		
		do {
			System.out.print("Need accesibility tools? (y/n) ");
			String in = input.nextLine();
			
			switch (in.toLowerCase()) {
			
			// If the user enters 'y', print out all the elements of which wheelChair's value is true.
			// And add all those elements into a new array (wheelChairGroup).
		    // If the array's length is equals to 0, it means no elements meet the demand.
				case "y":
				case "yes":
					System.out.println("The stations with wheelchairs include...");
					for (int i=0; i < station.length; i++) {
						if (station[i].hasWheelchair() == true) {
							System.out.println(station[i].toString());
							if (station.length > wheelChairGroup.length) {
								wheelChairGroup = resize(wheelChairGroup, wheelChairGroup.length*2);
							}
							wheelChairGroup[count] = station[i];
							count++;
						}
					}
					wheelChairGroup = resize(wheelChairGroup, count);
					
					if (wheelChairGroup.length == 0) {
						System.out.println("No stations are found!");
					}
					wheelChairGroup = null;
					
					done = true;
					break;
				
				// If the user enters 'n', print out all the elements of which wheelChair's value is false, 
				// though it's a little odd here. Maybe "with/without accessibility" would be better.
				case "n":
				case "no":
					System.out.println("The stations without wheelchairs include...");
					for (int i=0; i < station.length; i++) {
						if (station[i].hasWheelchair() == false) {
							System.out.println(station[i].toString());
							
							if (station.length > wheelChairGroup.length) {
								wheelChairGroup = resize(wheelChairGroup, wheelChairGroup.length*2);
							}
							wheelChairGroup[count] = station[i];
							count++;
						}	
					}
					wheelChairGroup = resize(wheelChairGroup, count);
				
					if (wheelChairGroup.length == 0) {
						System.out.println("No stations are found!");
					}
					wheelChairGroup = null;
					done = true;
					break;
					
				default:
					System.out.print("Invalid input. Try again. ");
			}
			
		} while (!done);
		
	}
	
	
	public static void displayNearest(Scanner input, CTAStation[] station) {
		
		double minDistance = 1000000;
		String minDistanceStation = "";
		
		// Prompt the user to input 2 numbers
		System.out.print("Enter your latitude and longitude (eg: -46.12,87.88): ");
		
		String in = input.nextLine();
		String[] userLocation = in.split(",");
		
			
		try {
			double userLocationLat = Double.parseDouble(userLocation[0]);
			double userLocationLnt = Double.parseDouble(userLocation[1]);
			
			// Check if the input is valid
			if (userLocation.length >= 3 || Math.abs(userLocationLat)>90 || Math.abs(userLocationLnt)>180) {
				System.out.println("Invalid input. Try again.");
			} else {
				
				// Iterate to find the minimum distance with the calcDistance() method
				for (int i=0; i<station.length; i++) {
					if (station[i].calcDistance(Double.parseDouble(userLocation[0]), 
							Double.parseDouble(userLocation[1])) <= minDistance) {
						minDistance = station[i].calcDistance(Double.parseDouble(userLocation[0]), 
								Double.parseDouble(userLocation[1]));
						minDistanceStation = station[i].getName();
					}
				}
				
				if (station.length != 0) {
					System.out.println("The nearest station is " + minDistanceStation);
				} else {
					System.out.println("No data is available.");
				}
				
			}
			
		} catch (Exception e) {
			System.out.println("Invalid input. Try again.");
		}
		 
	}
	
	// Create an array resizing method
	public static CTAStation[] resize(CTAStation[] data, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i < limit ; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}

}
