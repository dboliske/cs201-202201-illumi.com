package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Temperature converter)

import java.util.Scanner;

public class temperatureConverter_Exercise4 {
	
	// Test table
	// input: a, 100; output: 37.77777
	// input: a, -200; output: -128.88888
	// input: b, -500; output: invalid input
	// input: b, 37; output: 98.600000
	// input: c; output: exit
	// input: a, a; output: invalid input
	// input: b, ee; output: invalid input
	// It's great!

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create a scanner for the user input
		boolean running = true; // initialize the variable for the flag controlled loop below
		
		  while (running) {
	        
			//print out the menu
			System.out.println("a. Convert the Fahrenheit to Celsius");
			System.out.println("b. Convert the Celsius to Fahrenheit");
			System.out.println("c. Exit");
			System.out.print("Enter a, b, or c: ");
				
			char option = input.nextLine().charAt(0);
			
			switch (option) {
			
			    // convert the Fahrenheit to Celsius;
				case 'a':
					System.out.print("Enter the �H value to be converted: ");
					if (input.hasNextDouble()) { // check if the user's input is a number. If it is, then convert the value.
					      double numCelsius = (Double.parseDouble(input.nextLine()) - 32)/1.8;
					      if (numCelsius >= -273.15) { // the temperature should not be below -273.15 degree centigrade
					    	  System.out.println("The temperature is " + numCelsius + "��");
						  } else {
							  System.out.println("The temperature cannot be below absolute zero!");
						  }
					} else {
					      System.out.println("Invalid input");
					}
					System.out.println();
				    break;
				
				// convert the Celsius to Fahrenheit;
				case 'b':
					System.out.print("Enter the �� value to be converted: ");
					if (input.hasNextDouble()) { // check if the user's input is a number. If it is, then convert the value.
					      double numFahrenheit = (Double.parseDouble(input.nextLine()))*1.8 + 32;
					      if (numFahrenheit >= -459.67) {
					    	  System.out.println("The temperature is " + numFahrenheit + "�H");
						  } else {
							  System.out.println("The temperature cannot be below absolute zero!");
						  }
					} else {
						  System.out.println("Invalid input");
					}
					System.out.println();
					break;
				
				// exit the program
				case 'c':
					System.out.println("Goodbye.");
					running = false;
					break;
					
				default :
				    System.out.println("Invalid input.");
				
			}
		  }
	  
	  	input.close();

	}

}
