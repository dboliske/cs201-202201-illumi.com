package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Convert inch to centimeter)

import java.util.Scanner;

public class inchToCentimeter_Exercise6 {

	public static void main(String[] args) {
		
		//Test Table
		//input: -9; output: invalid input
		//input: 20; output: 50.8
		//input: a; output: invalid input
		//input: 89.99; output: 228.5846
		//input: 1000; output: 2540.0
		
		Scanner inchInput = new Scanner(System.in); // create a scanner for the user input

		System.out.print("Input the value in inches to be converted: ");
		
		if (inchInput.hasNextDouble()) { // check if the user's input is a number
			String inchInput_str = inchInput.nextLine(); // prompt the user to input a value
			
			if (Double.parseDouble(inchInput_str) >= 0) { // check if the input number is positive
				System.out.println(inchInput_str + " inches is equal to " + Double.parseDouble(inchInput_str)*2.54 + " centimeters."); // print out the value
			} else {
				System.out.println("Invalid input");
			}
			
		} else {
			System.out.println("Invalid input");
		}
		
		inchInput.close();
	}

}
