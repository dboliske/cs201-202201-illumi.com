package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Echo an input)

import java.util.Scanner;

public class inputAndEcho_Exercise1 {

	public static void main(String[] args) {
		
		//Writing a Java program that will prompt a user for a name; save the input and echo the name to the console.
		
		Scanner input = new Scanner(System.in); // Create a scanner for user input
		System.out.print("Enter your name: ");
		
		String echo = input.nextLine(); // prompt the user to input his/her name
		System.out.println("Your name is " + echo); // echo the user's input

		input.close();
	}

}
