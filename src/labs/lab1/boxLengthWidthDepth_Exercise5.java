package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Calculate the surface area of a box)

import java.util.Scanner;

public class boxLengthWidthDepth_Exercise5 {

	public static void main(String[] args) {
		
		//Test Table
		//input: -5, 7, 8; output: invalid input
		//input: a, b, 9; output: invalid input
		//input: 20, 21, 40; output: 4120.0
		//input: 22.9, 20, 24.8; output: 3043.84
		//input: 0, 9, 10; output: 180.0
		
		Scanner input = new Scanner(System.in); //creat a scanner for the user input

		System.out.print("Input the length of the box (ft): ");
		
		if (input.hasNextDouble()) { // Check if the input will be a number
		    double length = input.nextDouble();
		    
		    // prompt the user to input the width
		    System.out.print("Input the width of the box (ft): ");
		    double width = input.nextDouble();
		    
		    // prompt the user to input the depth
		    System.out.print("Input the depth of the box (ft): ");
		    double depth = input.nextDouble();
		    
		    // Check if all the inputs are positive, and output the surface area result
		    if (length >=0 && width >=0 && depth >= 0) { 
		    	System.out.print("The area of the wood needed to make the box should be at least " + 2* (length * width + length * depth + width * depth) + " square feet.");
		    } else {
		    	System.out.print("All the number must be positive.");
		    }
		    
		 } else {
		    System.out.println("Invalid input.");
		 }
		
		input.close();

	}

}

