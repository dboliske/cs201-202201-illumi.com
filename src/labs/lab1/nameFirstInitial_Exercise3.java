package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Output user's first initial)

import java.util.Scanner;

public class nameFirstInitial_Exercise3 {

	public static void main(String[] args) {
		
		//Prompt a user for a first name; display the user's first initial to the screen.
		Scanner name = new Scanner(System.in); // create a scanner for the user input
		
		System.out.print("Enter your first name: ");
		char firstInitial = name.nextLine().charAt(0); // pick the first letter of the user's input
		System.out.println("Your first initial is " + firstInitial); // print out that letter
		
		name.close();
	}

}
