package labs.lab1;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-01), name of program (Arithmetic Calculation)


public class arithmeticCalculation_Exercise2 {

	public static void main(String[] args) {
		
		// My age subtracted from my father's age
		int fathersAge = 62;
		int myAge = 34;
		int ageDifference = fathersAge - myAge;
		System.out.println("My father is " + ageDifference + " years older than me.");
				
		// My birth year multiplied by 2
		int myBirthYear = 1986;
		int doubleMyBirthYear = myBirthYear * 2;
		System.out.println("My birth year multiplied by 2 equals to " + doubleMyBirthYear);
		
		// Convert my height in inches to cms
		double myHeight_inch = 66.5;
		double myHeight_cm = myHeight_inch * 2.54;
		System.out.println("My height is " + myHeight_cm + " cm.");
		
		// Convert my height in inches to feet and inches where inches is an integer
		double myHeight_feet = myHeight_inch * 1/12;
		System.out.println("My height is "+ (int)myHeight_feet + " feet " + (int)myHeight_inch % 12 + " inch.");
		
		
		
	}

}
