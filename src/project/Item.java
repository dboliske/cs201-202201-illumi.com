package project;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//Item Class, super class of ProducedItem and AgeRestrictedItem

public class Item {
	
	private String name;
	private Double price;
	
	public Item() {
		name = "pencil";
		price = 1.29;
	}
	
	public Item(String name, Double price) {
		this.name = name;
		setPrice(price);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		if(price < 0) {
			this.price = 9999.99; // Get high price when the setting's invalid.
		} else {
			this.price = price;
		}
	}
	
	public String toString() {
		return name + " - " + "$" + price;
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Item)) {
			return false;
		}
		
		Item i = (Item)obj;
		if(!this.name.equals(i.getName())) {
			return false;
		} else if (this.price.equals(i.getPrice())) {
			return false;
		}
		
		return true;
		
	}
	
	protected String csvData() {
		// TO see if it's necessary
		return "-1";
	}
	
	public String writeToFile() {
		return name+","+price;
	}

}
