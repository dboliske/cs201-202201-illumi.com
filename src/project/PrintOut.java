package project;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//An enum class for most of the user interface
//And file path where reading and saving file is also here - filePath

public enum PrintOut {
	
	note("Note: "
			+ "\nThis is a self-checkout system for customers and self-service"
			+ "\ninventory management system for shopkeepers. Accessing the"
			+ "\nlatter one needs the admin password. Set up a password when"
			+ "\naccessing the inventory system for the first time.\n"),
	invalidInput ("\n- Invalid input. Returning to menu. -\n"), 
	returnBackToMenu("\n    Returning back to menu...\n"),
	addDone ("   - Add item(s) successfully. Returning to menu. -"),
	removeDone("   - Remove the item successful. Returning to menu. -"),
	cannotFind("   - Can't find the item you searched. Returning to menu. -"),
	
	mainMenu("\n1. Self-Checkout (for Customers) \n2. Inventory Management (for Shopkeepers) \n3. Save and Exit \n* Select a Service (1, 2, 3): "),
	customSecondaryMenu("\n  A. List All the Items \n  B. Buy or Search for Specific Items \n  C. Check Shopping Cart or Checkout \n  D. Return to Main Menu \n  * Select a Service (A - D): "),
	level3Menu("\n    i.   add more items to cart \n    ii.  check shopping cart \n    iii. remove all items from shopping cart \n    iv.  return to previous menu \n   * Select a Service (i - iv): "),
	inventorySecondaryMenu("\n  A. Change Password \n  B. List All the Items \n  C. Commodity Warehousing (ADD ITEMs)  "
			+ "\n  D. Remove Items \n  E. Search for Specific Items \n  F. Modify Items Attributes (Modify Price or More) \n  G. Return to Main Menu \n  * Select a Service (A - G): "),
	askForExpiDate("   * Enter the expiration date of the item (MM/DD/YYYY)\n    (eg. 09/19/2022): "),
	
	askForRestrictedAge("\n   * Enter the restricted age of the item (0-99): "),
	askForFastFood("   * Does the fast food need to be heated or not? (y/n) "),
	askForItemName("\n   - Add item(s) now -\n   * Item name: "),
	askForItemPrice("   * Item price: "),
	askForItemType("\n   a. Produce Items \n   b. Age Restricted Items \n   c. Fast Food \n   d. Others (Shelved Items) \n   * Choose the type of the item (a - d): "),
	askForItemQuantity("   * The quantity of the item putting in storage: "),
	askForSearchKeyword("\n   * Enter the item name you wanna search for: "),
	askForRemoveKeyword("\n   * Enter the item name to be removed: "),
	exitGoodbye("\n- Goodbye! -"),
	exitConfirm("\n- All the data will be saved. - \n* EXIT THE PROGRAM NOW? (y / n) "),
	saveFailed("- Error saving to file. -"),
	errorReadInData("\n  - Error occurred reading in data. -"),
	errorReadInFile("\n  - Error occurred reading in file. -"),
	
	wrongPassword(" - WRONG password. Returning back to main menu... -\n"),
	noPasswordFound("\n - No administrator password was created. -\n * Set up a new administrator password: "),
	passwordSetupSuccessfully("\n - Set up the password successfully. -\n"),
	askForPassword("\n * Enter the administrator password: "),
	askForOldPassword("\n   * Enter the old password: "),
	askForNewPassword("   * Enter a new password: "),
	askForNewPasswordWithNoPassword("\n   * No password was created. Set up a new password: "),
	passwordFailedtoSetup("   - The password can't be created. Contact the administrator. -"),
	passwordCannotBeEmpty(" - The password cann't be empty. -"),
	
	askForCheckOut("\n\n   * Check out? (y/n) "),
	nothingInShoppingCart("\n    - Nothing's in shopping cart. - \n"),
	noSearchedItemInStock("\n  - No such item in stock now. Returning to menu. -"),
	shoppingCartCleared("\n    - All the items have been removed from shopping cart. -"),
	itemInShoppingCart("\n    Item(s) in the shopping cart: "),
	askForSearchKeyWord2("\n   * Enter and search for item name to continue \n   (or enter 'done' to return to menu): "),
	listItems("\n  All the items in stock: "),
	itemRemoved("\n  - All the items above removed. -"),
	
	modifyItemNameTo("\n   * Change item name to: "),
	modifyItemPriceTo("   * Change item price to: "),
	modifyItemExiDateTo("   * Change expiration date to (eg. 04/29/2022): "),
	modifyFastfoodHeatOrNotTo("   * Need to be heated or not? (y/n): "),
	modifyRestrictedAgeTo("   * Change item restricted age to (eg. 18): "),
	modifySuccessfully("   - The item has been modified successfully. -\n"),
	modifySuccessfully2("   - Modified successfully. -\n"),
	NoItemModified("   0 item modified. Try other item name.\n"),
	cannotModifyInBatch("\n   - The items above cannot be modified in batch. -\n    - Some items' type is different from others'. -\n"),
	
	filePath("src/project/");
	
	private String message;
	
	PrintOut(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
