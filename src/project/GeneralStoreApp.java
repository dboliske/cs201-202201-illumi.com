package project;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//Application Class of the general store

public class GeneralStoreApp {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Item> data = readFile(PrintOut.filePath.getMessage()+"stock.csv");
		menu(data, input);
	}

	
	// customer menu, self-checkout system for customers
	public static ArrayList<Item> customerMenu(ArrayList<Item> data, Scanner input) {
		boolean customerDone = false;
		
		do {
			System.out.print(PrintOut.customSecondaryMenu.getMessage());
			String customOption = input.nextLine();
			
			switch (customOption.toLowerCase()) {
				case "a": // list all the items
					listItems(data);
					break;
				
				case "b": // search and add items to shopping cart
					data = addToCart(data, input);
					break;
				
				case "c": // check the shopping cart and check-out
					data = level3Menu(data, input);
					break;
					
				case "d": // return back to main menu
					customerDone = true;
					break;
				
				default:
					System.out.print(" " + PrintOut.invalidInput.getMessage());
			}
			
		} while(!customerDone);
		
		return data;
	}
	
	// submenu of the customer menu, mainly for shopping cart and checkout
	public static ArrayList<Item> level3Menu(ArrayList<Item> data, Scanner input) {
		
		boolean done = false;
		
		do {
			System.out.print(PrintOut.level3Menu.getMessage());
			String value = input.nextLine();
			
			switch (value.toLowerCase()) {
				case "i": // buy more items
				case "1":
					data = addToCart(data, input);
					break;
				
				case "ii": // checkout
				case "2":
					ArrayList<Item> cart = new ArrayList<Item>();

					cart = readFile(PrintOut.filePath.getMessage()+"cart.csv");
					if (cart.size() == 0) {
						System.out.print(PrintOut.nothingInShoppingCart.getMessage());
					} else {
						System.out.print(PrintOut.itemInShoppingCart.getMessage());
						for (int i=0; i<cart.size(); i++) {
							System.out.print("\n     " + cart.get(i));
						}
						
						System.out.print(PrintOut.askForCheckOut.getMessage());
						String in = input.nextLine();
						switch (in) {
							case "n":
							case "no":
								break;
							
							case "y":
							case "yes":
								double total = 0;
								for (int i=0; i<cart.size(); i++) {
									total += cart.get(i).getPrice();
								}
								if (total != 0) {
									System.out.print("\n    Total Price: $" + total +"\n    - Pay Successfully -\n");
									cart.clear();
									saveFile(PrintOut.filePath.getMessage()+"cart.csv", cart);
									done = true;
								} else {
									System.out.print(PrintOut.returnBackToMenu.getMessage());
								}
								break;
							
							default:
								
						}
					}
					
					break;
					
				case "iii": // clear shopping cart
				case "3":
					cart = readFile(PrintOut.filePath.getMessage()+"cart.csv");
					for (int i=0; i<cart.size(); i++) {
						data.add(cart.get(i));
					}
					cart.clear();
					System.out.println(PrintOut.shoppingCartCleared.getMessage());
					saveFile(PrintOut.filePath.getMessage()+"cart.csv", cart);
					saveFile(PrintOut.filePath.getMessage()+"stock.csv", data);
					break;
				
				case "iv": // get back to previous menu
				case "4":
					done = true;
					break;
				
				default:
					System.out.print("    " + PrintOut.invalidInput.getMessage());
			}
		} while(!done);
		
		return data;
	}
	
	// inventory menu, inventory management system for shopkeepers
	public static ArrayList<Item> inventoryMenu(ArrayList<Item> data, Scanner input) {

		boolean inventoryDone = false;
		
		boolean pass = checkPassword(PrintOut.filePath.getMessage()+"password.txt");
		if(pass) {
			do {
				System.out.print(PrintOut.inventorySecondaryMenu.getMessage());
				String inventoryOption = input.nextLine();
				
				switch (inventoryOption.toLowerCase()) {
					case "a":	// change password
						changePassword(PrintOut.filePath.getMessage()+"password.txt");
						break;
					
					case "b":	// list all the items
						listItems(data);
						break;
					
					case "c":	// add items
						data = addItem(data, input);
						break;
					
					case "d":	// remove items
						System.out.print(PrintOut.askForRemoveKeyword.getMessage());
						data = removeItem(data, input);
						break;
					
					case "e":	// search for items
						System.out.print(PrintOut.askForSearchKeyword.getMessage());
						searchItem(data, input);
						break;
					
					case "f":	// edit items
						data = modifyProgram(data, input);
						break;
					
					case "g":	// back to main menu
						inventoryDone = true;
						break;
					
					default:
						System.out.print("  "+PrintOut.invalidInput.getMessage());
				}
			} while (!inventoryDone);
		}
		
		return data;
	}
	
	// main menu of the system...
	public static void menu(ArrayList<Item> data, Scanner input) {
		
		System.out.print(PrintOut.note.getMessage());
		boolean done = false;
		
		do {
			System.out.print(PrintOut.mainMenu.getMessage());
			String option = input.nextLine();
			
				switch (option) {
					case "1":
						data = customerMenu(data, input);
						break;
					
					case "2":
						data = inventoryMenu(data, input);
						break;
					
					case "3":
						System.out.print(PrintOut.exitConfirm.getMessage());
						String exit = input.nextLine();
						switch (exit.toLowerCase()) {
							case "y":
							case "yes":
								exitProgram(data);
								done = true;
								break;
							
							case "n":
							case "no":
								break;
							
							default:
								System.out.print(PrintOut.invalidInput.getMessage());
							
						}
						break;
					
					default:
						System.out.print(PrintOut.invalidInput.getMessage());
			
				}
		} while(!done);

	}
	
	
	
// all the modifying methods are for modifying any item in stock
	// modify a single FastFoodItem
	public static ArrayList<Item> modifyFastFoodItem(ArrayList<Item> data, int num, Scanner input) {
		FastFoodItem f = (FastFoodItem)(data.get(num-1));
		System.out.print(PrintOut.modifyItemNameTo.getMessage());
		String modifiedName = input.nextLine();
		f.setName(modifiedName);
		System.out.print(PrintOut.modifyItemPriceTo.getMessage());
		String modifiedPrice = input.nextLine();
		try {
			f.setPrice(Double.parseDouble(modifiedPrice));
		} catch (Exception e) {
			System.out.print("\n   " + PrintOut.invalidInput.getMessage());
			return data;
		}
		
		System.out.print(PrintOut.modifyItemExiDateTo.getMessage());
		String modifiedExpiDate = input.nextLine();
		Date d = new Date(modifiedExpiDate);
		f.setExpiDate(d);
		
		System.out.print(PrintOut.modifyFastfoodHeatOrNotTo.getMessage());
		String modifiedHeatOrNot = input.nextLine();
		switch (modifiedHeatOrNot.toLowerCase()) {
			case "y":
			case "yes":
			case "true":
			case "t":
				f.setHeatOrNot(true);
				System.out.print(PrintOut.modifySuccessfully.getMessage());
				break;
				
			case "n":
			case "no":
			case "false":
			case "f":
				f.setHeatOrNot(false);
				System.out.print(PrintOut.modifySuccessfully.getMessage());
				break;
				
			default:
				System.out.print("\n   " + PrintOut.invalidInput.getMessage());
				return data;
		}
		
		return data;
	}
	// modify a single ProducedItem
	public static ArrayList<Item> modifyProduceItem(ArrayList<Item> data, int num, Scanner input) {
		ProducedItem p = (ProducedItem)(data.get(num-1));
		System.out.print(PrintOut.modifyItemNameTo.getMessage());
		String modifiedName = input.nextLine();
		p.setName(modifiedName);
		System.out.print(PrintOut.modifyItemPriceTo.getMessage());
		String modifiedPrice = input.nextLine();
		try {
			p.setPrice(Double.parseDouble(modifiedPrice));
		} catch (Exception e) {
			System.out.print("\n   " + PrintOut.invalidInput.getMessage());
			return data;
		}
		
		System.out.print(PrintOut.modifyItemExiDateTo.getMessage());
		String modifiedExpiDate = input.nextLine();
		Date d = new Date(modifiedExpiDate);
		p.setExpiDate(d);
		System.out.print(PrintOut.modifySuccessfully.getMessage());
		
		return data;
	}
	// modify a single AgeRestrictedItem
	public static ArrayList<Item> modifyAgeRestrictedItem(ArrayList<Item> data, int num, Scanner input) {
		AgeRestrictedItem a = (AgeRestrictedItem)(data.get(num-1));
		System.out.print(PrintOut.modifyItemNameTo.getMessage());
		String modifiedName = input.nextLine();
		a.setName(modifiedName);
		System.out.print(PrintOut.modifyItemPriceTo.getMessage());
		String modifiedPrice = input.nextLine();
		try {
			a.setPrice(Double.parseDouble(modifiedPrice));
		} catch (Exception e) {
			System.out.print("\n   " + PrintOut.invalidInput.getMessage());
			return data;
		}
		
		System.out.print(PrintOut.modifyRestrictedAgeTo.getMessage());
		String m = input.nextLine();
		try {
			int modifiedRestrictedAge = Integer.parseInt(m);
			a.setRestrictedAge(modifiedRestrictedAge);
			System.out.print(PrintOut.modifySuccessfully.getMessage());
		} catch (Exception e) {
			System.out.print("   " + PrintOut.invalidInput.getMessage());
			return data;
		}
		
		return data;
	}
	// modify ProducedItems in batches
	public static ArrayList<Item> batchModifyProduceItem(ArrayList<Item> data, ArrayList<Integer> quantityFound, Scanner input) {
		try {
			ArrayList<ProducedItem> p = new ArrayList<ProducedItem>();
			for (int i=0; i<quantityFound.size(); i++) {
				 p.add((ProducedItem)(data.get(quantityFound.get(i))));
			}	
			System.out.print(PrintOut.modifyItemNameTo.getMessage());
			String modifiedName = input.nextLine();
			for (int i=0; i<quantityFound.size(); i++) {
				p.get(i).setName(modifiedName);
			}
			System.out.print(PrintOut.modifyItemPriceTo.getMessage());
			String modifiedPrice = input.nextLine();
			try {
				for (int i=0; i<quantityFound.size(); i++) {
					p.get(i).setPrice(Double.parseDouble(modifiedPrice));
				}
			} catch (Exception e) {
				System.out.print("\n   " + PrintOut.invalidInput.getMessage());
				return data;
			}
			System.out.print(PrintOut.modifyItemExiDateTo.getMessage());
			String modifiedExpiDate = input.nextLine();
			Date d = new Date(modifiedExpiDate);
			for (int i=0; i<quantityFound.size(); i++) {
				p.get(i).setExpiDate(d);
			}
			System.out.print(PrintOut.modifySuccessfully2.getMessage());
		} catch (Exception e) {
			System.out.print(PrintOut.cannotModifyInBatch.getMessage());
			return data;
		}

		
		return data;
	}
	// modify AgeRestrictedItems in batches
	public static ArrayList<Item> batchModifyAgeRestrictedItem(ArrayList<Item> data, ArrayList<Integer> quantityFound, Scanner input) {
		try {
			ArrayList<AgeRestrictedItem> a = new ArrayList<AgeRestrictedItem>(); 
			for (int i=0; i<quantityFound.size(); i++) {
				 a.add((AgeRestrictedItem)(data.get(quantityFound.get(i))));
			}	
			System.out.print(PrintOut.modifyItemNameTo.getMessage());
			String modifiedName = input.nextLine();
			for (int i=0; i<quantityFound.size(); i++) {
				a.get(i).setName(modifiedName);
			}
			System.out.print(PrintOut.modifyItemPriceTo.getMessage());
			String modifiedPrice = input.nextLine();
			try {
				for (int i=0; i<quantityFound.size(); i++) {
					a.get(i).setPrice(Double.parseDouble(modifiedPrice));
				}
			} catch (Exception e) {
				System.out.print("\n   " + PrintOut.invalidInput.getMessage());
				return data;
			}
			System.out.print(PrintOut.modifyRestrictedAgeTo.getMessage());
			String m = input.nextLine();
			try {
				int modifiedRestrictedAge = Integer.parseInt(m);
				for (int i=0; i<quantityFound.size(); i++) {
					a.get(i).setRestrictedAge(modifiedRestrictedAge);
				}
				System.out.print(PrintOut.modifySuccessfully2.getMessage());
			} catch (Exception e) {
				System.out.print("   " + PrintOut.invalidInput.getMessage());
				return data;
			}
		} catch (Exception e) {
			System.out.print(PrintOut.cannotModifyInBatch.getMessage());
			return data;
		}	
		return data;

	}
	// modify FastFoodItems in batches
	public static ArrayList<Item> batchModifyFastFood(ArrayList<Item> data, ArrayList<Integer> quantityFound, Scanner input) {
		try {
			ArrayList<FastFoodItem> f = new ArrayList<FastFoodItem>();
			for (int i=0; i<quantityFound.size(); i++) {
				 f.add((FastFoodItem)(data.get(quantityFound.get(i))));
			}	
			System.out.print(PrintOut.modifyItemNameTo.getMessage());
			String modifiedName = input.nextLine();
			for (int i=0; i<quantityFound.size(); i++) {
				f.get(i).setName(modifiedName);
			}
			System.out.print(PrintOut.modifyItemPriceTo.getMessage());
			String modifiedPrice = input.nextLine();
			try {
				for (int i=0; i<quantityFound.size(); i++) {
					f.get(i).setPrice(Double.parseDouble(modifiedPrice));
				}
			} catch (Exception e) {
				System.out.print("\n   " + PrintOut.invalidInput.getMessage());
				return data;
			}
			
			System.out.print(PrintOut.modifyItemExiDateTo.getMessage());
			String modifiedExpiDate = input.nextLine();
			Date d = new Date(modifiedExpiDate);
			for (int i=0; i<quantityFound.size(); i++) {
				f.get(i).setExpiDate(d);
			}
			
			System.out.print(PrintOut.modifyFastfoodHeatOrNotTo.getMessage());
			String modifiedHeatOrNot = input.nextLine();
			switch (modifiedHeatOrNot.toLowerCase()) {
				case "y":
				case "yes":
				case "true":
				case "t":
					for (int i=0; i<quantityFound.size(); i++) {
						f.get(i).setHeatOrNot(true);
					}
					System.out.print(PrintOut.modifySuccessfully2.getMessage());
					break;
					
				case "n":
				case "no":
				case "false":
				case "f":
					for (int i=0; i<quantityFound.size(); i++) {
						f.get(i).setHeatOrNot(false);
					}
					System.out.print(PrintOut.modifySuccessfully2.getMessage());
					break;
					
				default:
					System.out.print("\n   " + PrintOut.invalidInput.getMessage());
					return data;
			}
		} catch (Exception e) {
			System.out.print(PrintOut.cannotModifyInBatch.getMessage());
			return data;
		}
			
		return data;
	}
	// main method of modifying program
	public static ArrayList<Item> modifyProgram(ArrayList<Item> data, Scanner input) {
		System.out.print("\n   * Enter the item name to be modified: ");		
		ArrayList<Integer> quantityFound = searchItem(data, input);
		
		if (quantityFound.size() == 0) {
			System.out.print(PrintOut.NoItemModified.getMessage());
		} else {
			System.out.print("\n   Choose an item number above to modify the item. (eg. " + (quantityFound.get(0)+1) +")\n   (or enter 'all' to modify all the items above in batch): ");
			String value = input.nextLine();
			
			switch (value.toLowerCase()) {
				case "all":
					if (data.get(quantityFound.get(0)) instanceof FastFoodItem) {
						data = batchModifyFastFood(data, quantityFound, input);
					} else if (data.get(quantityFound.get(0)) instanceof ProducedItem) {
						data = batchModifyProduceItem(data, quantityFound, input);
					} else if (data.get(quantityFound.get(0)) instanceof AgeRestrictedItem) {
						data = batchModifyAgeRestrictedItem(data, quantityFound, input);
					} else {
						System.out.print(PrintOut.modifyItemNameTo.getMessage());
						String modifiedName = input.nextLine();
						for (int i=0;i<quantityFound.size(); i++) {
							data.get(i).setName(modifiedName);
						}
						System.out.print(PrintOut.modifyItemPriceTo.getMessage());
						String modifiedPrice = input.nextLine();
						try {
							for (int i=0; i<quantityFound.size(); i++) {
								data.get(i).setPrice(Double.parseDouble(modifiedPrice));
							}
							System.out.print(PrintOut.modifySuccessfully2.getMessage());
						} catch (Exception e) {
							System.out.print("\n   " + PrintOut.invalidInput.getMessage() + "\n");
							return data;
						}
					}
					break;
				
				default:
					try {
						int num = Integer.parseInt(value);
						if (data.get(num-1) instanceof FastFoodItem) {
							data = modifyFastFoodItem(data, num, input);
						} else if (data.get(num-1) instanceof ProducedItem) {
							data = modifyProduceItem(data, num, input);
						} else if (data.get(num-1) instanceof AgeRestrictedItem) {
							data = modifyAgeRestrictedItem(data, num, input);
						} else {
							System.out.print(PrintOut.modifyItemNameTo.getMessage());
							String modifiedName = input.nextLine();
							data.get(num-1).setName(modifiedName);
							System.out.print(PrintOut.modifyItemPriceTo.getMessage());
							String modifiedPrice = input.nextLine();
							try {
								data.get(num-1).setPrice(Double.parseDouble(modifiedPrice));
								System.out.print(PrintOut.modifySuccessfully.getMessage());
							} catch (Exception e) {
								System.out.print("\n   " + PrintOut.invalidInput.getMessage());
								return data;
							}
						}
					} catch (Exception e) {
						System.out.print("\n   " + PrintOut.invalidInput.getMessage() + "\n");
						return data;
					}
					return data;	
			}
		}
		
		return data;
	}
	

	
	// add one or more items to shopping cart, and remove them from the inventory
	public static ArrayList<Item> addToCart(ArrayList<Item> data, Scanner input) {
		
		boolean done = false;
		do {
			
			// search for the item name from user input
			System.out.print(PrintOut.askForSearchKeyWord2.getMessage());
			sortItem(data);

			String value = input.nextLine();
			
			if (!value.equals("done")) {
				ArrayList<Integer> quantityFound = new ArrayList<Integer>();
				int start = 0;
				int end = data.size();
				boolean found = false;
				while (!found && start != end) {
					int middle = (start + end)/2;
					if (data.get(middle).getName().equalsIgnoreCase(value)) {
						found = true;
						for (int i=start; i<end; i++) {
							if(data.get(i).getName().equalsIgnoreCase(value)) {
								quantityFound.add(i);
							}
						}
						
					} else if (data.get(middle).getName().compareToIgnoreCase(value) < 0) {
						start = middle + 1;
					} else {
						end = middle;
					}
					
				}
				
				System.out.println("   " + quantityFound.size() + " item(s) found. ");
				for (int i=0; i<quantityFound.size(); i++) {
					System.out.println("    No." + (quantityFound.get(i)+1) + " - " + data.get(quantityFound.get(i)).toString());
				}
				
				
				// add the item to cart, and remove the corresponding item from inventory 
				if (quantityFound.size() == 0) {
					done = true;
				} else {
					ArrayList<Item> itemIntoCart = readFile(PrintOut.filePath.getMessage()+"cart.csv");
					System.out.print("\n   Choose an item number above to add the item to cart (eg: " + (quantityFound.get(0) + 1) + ")\n   (or enter 'done' to return to menu): ");
					value = input.nextLine();
					if (value.equals("done")) {
						done = true;
					} else {
						try {
							int itemNo = Integer.parseInt(value);
							itemIntoCart.add(data.get(itemNo-1));
							data.remove(itemNo-1);
							saveFile(PrintOut.filePath.getMessage()+"cart.csv", itemIntoCart);
							saveFile(PrintOut.filePath.getMessage()+"stock.csv", data);
							System.out.print("\n   - The No." + itemNo + " item has been added to shopping cart. -\n");
						} catch (Exception e) {
							System.out.print("\n   - Invalid input. Try Again. -\n");
						}
					}
				
				}
		
			} else {
				done = true;
			}

		} while(!done);
		
		return data;
		
	}
	
	// list all the items in stock
	public static void listItems(ArrayList<Item> data) {
		System.out.println(PrintOut.listItems.getMessage());
		for (int i=0; i<data.size(); i++) {
			System.out.println("   No."+(i+1) + " - " +data.get(i));
		}
	}
	
	// To improve the searching efficiency, it is neccessary to sort items - with selection sorting algorithm.
	public static ArrayList<Item> sortItem(ArrayList<Item> data) {
		for (int i=0; i<data.size() - 1; i++) {
			int min = i;
			for (int j=i+1; j<data.size(); j++) {
				if(data.get(j).getName().compareToIgnoreCase(data.get(min).getName()) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				Item temp = data.get(i);
				data.set(i, data.get(min));
				data.set(min, temp);
			}
		}
		
		return data;
	}
	
	// Searching for the items
	// Binary search + sequantial search to list all the items matching
	public static ArrayList<Integer> searchItem(ArrayList<Item> data, Scanner input) {
		
		sortItem(data);
		
		String value = input.nextLine();
		
		ArrayList<Integer> quantityFound = new ArrayList<Integer>();
		int start = 0;
		int end = data.size();
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end)/2;
			if (data.get(middle).getName().equalsIgnoreCase(value)) {
				found = true;
				for (int i=start; i<end; i++) {
					if(data.get(i).getName().equalsIgnoreCase(value)) {
						quantityFound.add(i);
					}
				}
				
			} else if (data.get(middle).getName().compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
			
		}
		
		System.out.println("   " + quantityFound.size() + " item(s) found. ");
		for (int i=0; i<quantityFound.size(); i++) {
			System.out.println("    No." + (quantityFound.get(i)+1) + " - " + data.get(quantityFound.get(i)).toString());
		}
		
		return quantityFound;

//		ArrayList<Integer> quantityFound = new ArrayList<Integer>();
//		for (int i=0; i<data.size(); i++) {
//			if (data.get(i).getName().equalsIgnoreCase(value)) {
//				quantityFound.add(i);
//			}
//		}
//		
//		return quantityFound;
	}
	
	// remove one or more items in batches
	public static ArrayList<Item> removeItem(ArrayList<Item> data, Scanner input) {
		ArrayList<Integer> quantityFound = searchItem(data, input);
		if (quantityFound.size() == 0) {
			System.out.println(PrintOut.noSearchedItemInStock.getMessage());
		} else {
			System.out.print("\n   Choose an item number above to remove the item (eg. "+ (quantityFound.get(0)+1) + ")," 
					+ "\n   or enter 'all' to remove all above: ");
			String value = input.nextLine();
			switch (value.toLowerCase()) {
				case "all": // remove all the items listed
					for (int i=0; i<quantityFound.size(); i++) {
						int o = quantityFound.get(0);
						data.remove(o); // remove the value at the same position several times given it's an ArrayList
					}
					System.out.println(PrintOut.itemRemoved.getMessage());
					break;
					
				default: // remove the item specified
					try {
						int n = Integer.parseInt(value);
						data.remove(n-1);
						System.out.println("\n  - the No." + n + " item removed. -");
					} catch (Exception e) {
						System.out.println("  "+PrintOut.invalidInput.getMessage());
						return data;
					}
					
			}
		}
		
		return data;
	}
	
	// add one or more items to inventory
	public static ArrayList<Item> addItem(ArrayList<Item> data, Scanner input) {
		Item i = null;
		System.out.print(PrintOut.askForItemName.getMessage());
		String name = input.nextLine();
		System.out.print(PrintOut.askForItemPrice.getMessage());
		Double price = 0.0;
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("  "+PrintOut.invalidInput.getMessage());
			return data;
		}
		
		System.out.print(PrintOut.askForItemType.getMessage());
		String type = input.nextLine();
		switch(type) {
			case "a":
				System.out.print(PrintOut.askForExpiDate.getMessage());
				String value1 = input.nextLine();
				Date date = new Date(value1);
				if (date.getMonth() != null) {
					i = new ProducedItem(name, price, date);
				} else {
					System.out.println("  "+PrintOut.invalidInput.getMessage());
					return data;
				}
				break;
			
			case "b":
				System.out.print(PrintOut.askForRestrictedAge.getMessage());
				String value2 = input.nextLine();
				try {
					int restrictedAge = Integer.parseInt(value2);
					i = new AgeRestrictedItem(name, price, restrictedAge);
				} catch(Exception e) {
					System.out.println("  "+PrintOut.invalidInput.getMessage());
					return data;
				}
				break;
			
			case "c":
				System.out.print(PrintOut.askForFastFood.getMessage());
				String value3 = input.nextLine();
				boolean needToBeHeated = true;
				switch (value3.toLowerCase()) {
					case "yes":
					case "y":
						needToBeHeated = true;
						break;
					
					case "no":
					case "n":
						needToBeHeated = false;
						break;
						
					default:
						System.out.println("  "+PrintOut.invalidInput.getMessage());
						return data;
				}
				System.out.print(PrintOut.askForExpiDate.getMessage());
				String value4 = input.nextLine();
				Date date2 = new Date(value4);
				if (date2.getMonth() != null) {
					i = new FastFoodItem(name, price, date2, needToBeHeated);
				} else {
					System.out.println("  "+PrintOut.invalidInput.getMessage());
					return data;
				}
				
				break;
			
			case "d":
				i = new Item(name, price);
				break;
			
			default:
				System.out.println("  "+PrintOut.invalidInput.getMessage());
				return data;
		}
		
		System.out.print(PrintOut.askForItemQuantity.getMessage());
		int itemQuantity = 0;
		try {
			itemQuantity = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("  "+PrintOut.invalidInput.getMessage());
			return data;
		}
		
		for (int j=0; j<itemQuantity; j++) {
			data.add(i);
		}
		System.out.println(PrintOut.addDone.getMessage());
		
		return data;
	}
	
	// read in file and put all the items into an ArrayList
	public static ArrayList<Item> readFile(String filename) {
		ArrayList<Item> items = new ArrayList<Item>();
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");

					Item i = null;
					switch (values.length) {
						case 2:
							i = new Item(values[0], Double.parseDouble(values[1]));
							break;
						
						case 4:
							Date d = new Date(values[2]);
							i = new FastFoodItem(values[0], Double.parseDouble(values[1]), d, Boolean.parseBoolean(values[3]));
							break;
						
						case 3:
							if(values[2].charAt(0) != '0') {
								i = new AgeRestrictedItem(values[0], Double.parseDouble(values[1]), Integer.parseInt(values[2]));
							} else {
								Date d1 = new Date(values[2]);
								i = new ProducedItem(values[0], Double.parseDouble(values[1]), d1);
							}
							break;		
					}
					
					items.add(i);
					
				} catch (Exception e) {
					System.out.println(PrintOut.errorReadInData.getMessage());
					return items;
				}
			}
			
		} catch (Exception e) {
			System.out.println(PrintOut.errorReadInFile.getMessage());
			return items;
		}
		
		return items;
	}
	
	// save all the data to the stock.csv file
	public static void saveFile(String filename, ArrayList<Item> data) {
		try {
			FileWriter writer = new FileWriter(filename);
			if (data == null) {
				writer.write("");
				writer.flush();
			} else {
				for (Item i : data) {
					writer.write(i.writeToFile() + "\n");
					writer.flush();
				}
				writer.close();
			}
		} catch (Exception e) {
			System.out.println(PrintOut.saveFailed.getMessage());
		}
	}
	
	// Shopkeepers have to enter the password to access the inventory system,
	// but customers don't have to enter the password to access the self-checkout system.
	// And I know it's unsafe to just store the plaintext password into a txt file.
	public static boolean checkPassword(String filename) {
		
		boolean pass = false;
		Scanner userInput = new Scanner(System.in);
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			String password = input.nextLine();
			
			System.out.print(PrintOut.askForPassword.getMessage());
			String tryPassword = userInput.nextLine();
			if(tryPassword.equals(password)) {
				pass = true;
			} else {
				System.out.print(PrintOut.wrongPassword.getMessage());
			}
			
		} catch (Exception e) {
			System.out.print(PrintOut.noPasswordFound.getMessage());
			String initialPassword = userInput.nextLine();
			if (!initialPassword.equals("")) {
				savePassword(filename, initialPassword);
				System.out.println(" " + PrintOut.passwordSetupSuccessfully.getMessage());
			} else {
				System.out.println(PrintOut.passwordCannotBeEmpty.getMessage());
			}
		}
		
		return pass;
	}
	
	// change the password to the inventory system
	public static void changePassword(String filename) {
		
		Scanner userInput = new Scanner(System.in);
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			String password = input.nextLine();
			
			System.out.print(PrintOut.askForOldPassword.getMessage());
			String oldPassword = userInput.nextLine();
			if (oldPassword.equals(password)) {
				System.out.print(PrintOut.askForNewPassword.getMessage());
				String newPassword = userInput.nextLine();
				if (!newPassword.equals("")) {
					savePassword(filename, newPassword);
					System.out.println(" " + PrintOut.passwordSetupSuccessfully.getMessage());
				} else {
					System.out.println(PrintOut.passwordCannotBeEmpty.getMessage());
				}
			} else {
				System.out.println(" " + PrintOut.wrongPassword.getMessage());
			}
			
		
		} catch (Exception e) {
			System.out.print(PrintOut.askForNewPasswordWithNoPassword.getMessage());
			String initialPassword = userInput.nextLine();
			if (!initialPassword.equals("")) {
				savePassword(filename, initialPassword);
				System.out.println(" " + PrintOut.passwordSetupSuccessfully.getMessage());
			} else {
				System.out.println(PrintOut.passwordCannotBeEmpty.getMessage());
			}

		}
		
	}
	
	// save the password to a txt file
	public static void savePassword(String filename, String password) {

		try {
			FileWriter writer = new FileWriter(filename);
			writer.write(password);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.out.println(PrintOut.passwordFailedtoSetup.getMessage());
		}
	}
	
	// When a user's trying to exit the program, all the data will be saved.
	// And all the items in the shopping cart will be removed from the shopping cart.
	// All the items in the shopping cart will be returned back to the inventory.
	public static void exitProgram(ArrayList<Item> data) {
		
		// clear customer shopping cart before exiting
		ArrayList<Item> cart = readFile(PrintOut.filePath.getMessage()+"cart.csv");
		for (int i=0; i<cart.size(); i++) {
			data.add(cart.get(i));
		}
		cart.clear();
		saveFile(PrintOut.filePath.getMessage()+"cart.csv", cart);
		saveFile(PrintOut.filePath.getMessage()+"stock.csv", data);
		System.out.println(PrintOut.exitGoodbye.getMessage());

	}

}
