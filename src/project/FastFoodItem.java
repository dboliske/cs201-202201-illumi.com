package project;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//FastFoodItem Class, subclass of ProducedItem, describes an item type which may require heating before being sold.

public class FastFoodItem extends ProducedItem {

	private boolean needToBeHeated;
	
	public FastFoodItem() {
		super();
		needToBeHeated = true;
	}
	
	public FastFoodItem(String name, Double price, Date expiDate, boolean needToBeHeated) {
		super(name, price, expiDate);
		this.needToBeHeated = needToBeHeated;
	}
	
	public boolean needToBeHeated() {
		return needToBeHeated;
	}
	
	public void setHeatOrNot(boolean needToBeHeated) {
		this.needToBeHeated = needToBeHeated;
	}
	
	@Override
	public String toString() {
		return super.toString() + " - " + (needToBeHeated?"require heating" : "not need heating");
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof FastFoodItem)) {
			return false;
		}
		
		FastFoodItem p = (FastFoodItem)obj;
		if (this.needToBeHeated != p.needToBeHeated) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String writeToFile() {
		return super.writeToFile() + "," + needToBeHeated;
	}
}
