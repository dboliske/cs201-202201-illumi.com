package project;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//AgeRestrictedItem Class, a subclass of Item Class, which describes an item type that has a restriction on purchasing it based on the age of the customer

public class AgeRestrictedItem extends Item {
	
	private int restrictedAge;
	
	public AgeRestrictedItem() {
		super();
		restrictedAge = 21;
	}
	
	public AgeRestrictedItem(String name, Double price, int restrictedAge) {
		super(name, price);
		setRestrictedAge(restrictedAge);
	}
	
	public int getRestrictedAge() {
		return restrictedAge;
	}
	
	public void setRestrictedAge(int r) {
		if(r<0 || r>90) {
			restrictedAge = 0;
		} else {
			restrictedAge = r;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + " - Restricted Age: " + restrictedAge;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem a = (AgeRestrictedItem)obj;
		if (this.restrictedAge != a.restrictedAge) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String writeToFile() {
		return super.writeToFile() + "," + restrictedAge;
	}

}
