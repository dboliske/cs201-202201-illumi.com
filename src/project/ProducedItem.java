package project;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//ProducedItem Class, a subclass of Item, for the item that has expiration date

public class ProducedItem extends Item {
	
	private Date expiDate;
	
	public ProducedItem() {
		super();
		expiDate = new Date("01/01/1970");
	}
	
	public ProducedItem(String name, Double price, Date expiDate) {
		super(name, price);
		this.expiDate = expiDate;
	}
	
	public Date getExpiDate() {
		return expiDate;
	}
	
	public void setExpiDate(Date date) {
		expiDate = date;
	}
	
	@Override
	public String toString() {
		return super.toString() + " - Expiration Date: " + expiDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ProducedItem)) {
			return false;
		}
		
		ProducedItem p = (ProducedItem)obj;
		if (!this.expiDate.equals(p.expiDate)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String writeToFile() {
		return super.writeToFile() + "," + expiDate;
	}
}
