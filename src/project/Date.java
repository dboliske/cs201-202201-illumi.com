package project;

import java.util.regex.Pattern;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-25), name of program (General Store App, Final Project)
//Date Class, for fomulating Item Class or its subclasses' Date expression

public class Date {
	private int year;
	private Month month;
	private int day;
	
	private static String dataPattern =
			"([0][1-9]|[1][0-2])(/)([0][1-9]|[12][0-9]|[3][01])(/)[0-9]{4}";
	private static String dataPattern2 =
			"([1-9]|[1][0-2])(/)([1-9]|[12][0-9]|[3][01])(/)[0-9]{4}";
	
	public Date() {
		year = 1970;
		month = Month.JANUARY;
		day = 1;
	}
	
	public Date(int year, int month, int day) {
		this();
		setYear(year);
		setMonth(month);
		setDay(day);
	}
	
	public Date(String d) {
		this();
		if(Pattern.matches(dataPattern, d) || Pattern.matches(dataPattern2, d)) {
			String[] values = d.split("/");
			setYear(Integer.parseInt(values[2]));
			setMonth(Integer.parseInt(values[0]));
			setDay(Integer.parseInt(values[1]));
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year < 1970 || year > 2100) {
			year = 1970;
		}
		if (year >= 1970 && year <= 2100){
			this.year = year;
		}
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(int month) {
		Month m = Month.getMonth(month);
		if(m == null) {
			this.month = Month.JANUARY;
		} else {
			this.month = m;
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day < 1 || day > month.numberOfDays()) {
			day = 1;
		} 
		if (day >= 1 && day <= month.numberOfDays()){
			this.day = day;
		}
		
	}
	
	public String toString() {
		return (month.mons()<10 ? ("0" + month.mons()) : month.mons()) + "/" + (day<10 ? ("0"+day) : day)
				+ "/" + year;
	}
	
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (!(obj instanceof Date)) {
			return false;
		}
		
		Date d = (Date)obj;
		if (year != d.getYear()) {
			return false;
		} else if (month != d.getMonth()) {
			return false;
		} else if (day != d.getDay()) {
			return false;
		}
		
		return true;
	}
	
	public boolean isBefore(Date date) {
		if (date.getYear() < year) {
			return false;
		} else if (date.getMonth().mons() < month.mons()) {
			return false;
		} else if (date.getDay() < day) {
			return false;
		}
		
		return true;
	}
	
	
}
