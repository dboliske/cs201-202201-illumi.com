package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Question Four: Sorting)

public class Sorting_Question4 {

	public static void main(String[] args) {
		String[] list = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", 
				"percentage", "height", "wealth", "resource", "lake", "importance"};
		list = sortList(list);
		
		// print out all the elements in the array
		for (int i=0; i<list.length - 1; i++) {
			System.out.print(list[i] + ", ");
		}
		System.out.print(list[list.length - 1]); // print out the last element of the array

	}
	
	// selection sort algorithm
	public static String[] sortList(String[] data) {
		for (int i=0; i<data.length - 1; i++) {
			int min = i;
			for (int j = i+1; j<data.length; j++) {
				if(data[j].compareTo(data[min]) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = data[i];
				data[i] = data[min];
				data[min] = temp;
			}
		}
		
		return data;
	}

}
