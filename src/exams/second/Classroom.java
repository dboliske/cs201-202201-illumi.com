package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Classroom Class, Question One: Inheritance/Polymorphism)

public class Classroom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		setBuilding("Minglun");
		setRoomNumber("#22");
		setSeats(14);
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats > 0) {
			this.seats = seats;
		} else {
			this.seats = 1;
		}
	}
	
	@Override
	public String toString() {
		return "Seats: " + seats + " - Room Number: " + roomNumber + " - Building: " + building;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Classroom)) {
			return false;
		}
		
		Classroom c = (Classroom)obj;
		if(this.building != null && !this.building.equals(c.getBuilding())) {
			return false;
		} else if (this.roomNumber != null && !this.roomNumber.equals(c.getRoomNumber())) {
			return false;
		} else if (this.seats != c.getSeats()) {
			return false;
		}
		
		return true;
	}
}
