package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (ComputerLab Class, subclass of Classroom, Question One: Inheritance/Polymorphism)

public class ComputerLab extends Classroom {
	
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	@Override
	public String toString() {
		return super.toString() + " - " + (computers ? "has computers" : "has no computers");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ComputerLab)) {
			return false;
		}
		
		ComputerLab cl = (ComputerLab)obj;
		if (this.computers != cl.hasComputers()) {
			return false;
		}
		
		return true;
	}
 }
