package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Polygon Class, Question Two: Abstract Classes)

public abstract class Polygon {
	
	protected String name;
	
	public Polygon() {
		name = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "Polygon's name: " + name;
	}
	
	public abstract double area();
	public abstract double perimeter();
}
