package exams.second;

import java.util.Scanner;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Question Five: Searching)

public class Searching_Question5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		double[] numList = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		numList = sortList(numList); // sort the array before searching
		
		do {
			System.out.print("Enter a number to be searched for: ");
			String in = input.nextLine();
			
			try {
				// search for the number that the user inputs
				int foundIndex = jumpSearch(numList, Double.parseDouble(in));
				if (foundIndex != -1) {
					System.out.println("The number you searched for is at #" + (foundIndex + 1) + " of the List.");
					done = true;
				} else {
					System.out.println("Error " + foundIndex + ": cannot find the number you searched for.");
					done = true;
				}
			} catch (Exception e) {
				if (in.equals("exit")) {
					done = true;
				} else {
					System.out.print("Invalid input. Try again! \n\n");
				}
			}

			
		} while (!done);
		
	}
	
	// jump research algorithm
	public static int jumpSearch(double[] array, double value) {
		int step = (int)Math.sqrt(array.length);
		int prev = 0;
		
		return jumpSearchRecursively(array, value, step, prev);
	}
	// jump search recursively
	public static int jumpSearchRecursively(double[] array, double value, int step, int prev) {
		if ((prev+step) < array.length && (array[prev+step] < value)) {
			prev += step + 1;	
			return jumpSearchRecursively(array, value, step, prev);
		} else {
			for (int i = prev; i <= (prev+step) && i<array.length; i++) {
				if (value == array[i]) {
					return i;
				}
			}
		}
		
		return -1;
	}
	
	
	// selection sort algorithm
	public static double[] sortList(double[] data) {
		for (int i=0; i<data.length - 1; i++) {
			int min = i;
			for (int j = i+1; j<data.length; j++) {
				if(data[j] < data[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				Double temp = data[i];
				data[i] = data[min];
				data[min] = temp;
			}
		}
		
		return data;
	}

}
