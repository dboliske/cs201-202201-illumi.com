package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Rectangle Class, Question Two: Abstract Classes)

public class Rectangle extends Polygon {
	
	private double width;
	private double height;
	
	public Rectangle() {
		name = "Rectangle";
		setWidth(1);
		setHeight(1);
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		if(height > 0) {
			this.height = height;
		}
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}
	
	@Override
	public String toString() {
		return super.toString() + " - " + "Type: Rectangle - Width: " + width + ", Height: " + height;
	}

}
