package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Question Three: ArrayLists)

public class ArrayList_Question3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Double> numList = new ArrayList<Double>();
		boolean done = false;
		
		
		System.out.print("Input any number below. And Enter 'done' when it's finished as you like. \n");
		do {
			// Prompt the user to input any number
			System.out.print("Input any number: ");
			String in = input.nextLine();
			
			// Add all the numbers to an ArrayList
			try {
				if (!in.toLowerCase().equals("done")) {
					numList.add(Double.parseDouble(in));
				} else if (in.toLowerCase().equals("done")) {
					done = true;
				}
			} catch (Exception e) {
				System.out.print("\n- Invalid input. Try again! -\n\n");
			}
			
		} while(!done);
		
		input.close();
		
		// Sort the list. The first number should be the minimum, and the last number is the maximum.
		if (numList.size() != 0) {
			numList = sortList(numList);
			System.out.print("\nThe maminum number is: " + numList.get(0) + ".\nThe maximum number is: " + numList.get(numList.size() - 1) + ".");
		}
	}
	
	// selection sort algorithm
	public static ArrayList<Double> sortList(ArrayList<Double> data) {
		for (int i=0; i<data.size() - 1; i++) {
			int min = i;
			for (int j = i+1; j<data.size(); j++) {
				if(data.get(j) < data.get(min)) {
					min = j;
				}
			}
			
			if (min != i) {
				double temp = data.get(i);
				data.set(i, data.get(min));
				data.set(min, temp);
			}
		}
		
		return data;
	}

}
