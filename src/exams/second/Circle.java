package exams.second;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-04-29)
//name of program (Circle Class, Question Two: Abstract Classes)

public class Circle extends Polygon {
	
	private double radius;
	
	public Circle() {
		name = "Circle";
		setRadius(1);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}

	@Override
	public double area() {
		return Math.PI * radius * radius;
	}

	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}
	
	@Override
	public String toString() {
		return super.toString() + " - " + "Type: Circle - Radius: " + radius;
	}

}
