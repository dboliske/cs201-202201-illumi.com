package exams.first;
//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-26), name of program (Question Five: Objects)
public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name = "Patty";
		age = 0;
	}
	
	public Pet(String name, int age) {
		this.name = name;
		this.age = 0;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if (name != p.name) {
			return false;
		} else if (age != p.age) {
			return false;
		}
		
		return true;
	}
	
	public String toString() {
		return name + "'s age is " + age;
	}

}
