package exams.first;

//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-26), name of program (Question Four: Arrays)

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[] array = new String[5]; // user's inputs array
		int counter = 0;
		
		
		System.out.println("Enter 5 words below: ");
		
		// Store the user's inputs to the array
		for (counter=0; counter<5; counter++) {
			System.out.print("Enter the #" + (counter + 1) + " word: ");
			array[counter] = input.nextLine();
		}
		
		input.close();
		
		// Enumerate to find the words that appear more than once,
		// and store those words to another array "sameWords"
		String[] sameWords = new String[100];
		int counterB = 0;
		int plus = 0;
		for (counter=0; counter + plus < array.length; counter++) {
			for (plus=1; counter + plus < array.length; plus++) {
				if (array[counter].equals(array[counter + plus])) {
					sameWords[counterB] = array[counter];
					counterB++;
				} 
			}
			plus = 1;
		}
		

		
		// Trim down the sameWords array;
		// 1.create temporary arrays
		String[] temp1 = new String[counterB];
		for (int i=0; i<temp1.length; i++) {
			temp1[i] = sameWords[i];
		}
		// 2.remove the values that are repetitive
		for (int i=0, j=0; i + j < temp1.length; i++) {
			for (j=1; i + j < temp1.length; j++) {
				if (temp1[i] != null && temp1[i].equals(temp1[i+j])) {
					temp1[i + j] = null;
				} 
			}
			j = 1;
		}
		// 3.transfer the elements of which the value is null to the end of the array;
		String[] temp2 = new String[counterB];
		counter = 0;
		for (int i=0; i<counterB; i++) {
			if (temp1[i] != null) {
				temp2[counter] = temp1[i];
				counter++;
			}
		}
		// 4.trim down the array
		String[] temp3 = new String[counter];
		for (int i=0; i<temp3.length; i++) {
			temp3[i] = temp2[i];
		}
		sameWords = temp3;
		temp3 = null;
		temp2 = null;
		temp1 = null;
		
		

		// Print out the same words
		if (sameWords.length == 2) {
			System.out.println("The words that appear more than once are " + sameWords[0] + " and " + sameWords[1]);
		} else if (sameWords.length == 1){
			System.out.println("The word that appears more than once is " + sameWords[0]);
		} else {
			System.out.println("No words are the same.");
		}
	}

}
