package exams.first;
//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-26), name of program (Question One: Data Types)
import java.util.Scanner;

public class DataType {

	public static void main(String[] args) {
		
		try {
			Scanner input = new Scanner(System.in);
			System.out.print("Enter an integer: ");
			
			int inputInt = Integer.parseInt(input.nextLine());
			System.out.print("The ASCII character correspoding to this number plus 65 is " + (char)(inputInt + 65) + ".");
			
			input.close();
		} catch (Exception e) {
			System.out.print("The input is invalid.");
		}
	

	}

}
