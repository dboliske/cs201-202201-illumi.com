package exams.first;
//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-26), name of program (Question Three: Repetition)
import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Now let's draw a triangle.");
		System.out.print("Enter a number to define the size: ");
		try {
			int inputIn = Integer.parseInt(input.nextLine());
			if (inputIn > 1) {
				for (int row = 1; row <= inputIn; row++) {
					for (int space = row - 1; space > 0; space--) {
						System.out.print("  ");
					}
					for (int col = inputIn - row; col >= 0; col--) {
						System.out.print("* ");
					}
					System.out.println();
				}
			} else {
				System.out.println("The number should be bigger than 1.");
			}
			
			input.close();
		} catch(Exception e) {
			System.out.println("Invalid input.");
		}

	}

}
