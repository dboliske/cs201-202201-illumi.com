package exams.first;
//name (Yefeng Huang), course (CS201), sec.# (03), date (2022-02-26), name of program (Question Two: Selection)
import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		
		try {
			int inputInt = Integer.parseInt(input.nextLine());
			
			if (inputInt % 2 == 0  && inputInt % 3 != 0) {
				System.out.println("foo");
			} else if (inputInt % 2 != 0 && inputInt % 3 == 0) {
				System.out.println("bar");
			} else if (inputInt % 2 == 0 && inputInt % 3 == 0) {
				System.out.println("foobar");
			} else {
				// do nothing
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Invalid input.");
		}

	}

}
